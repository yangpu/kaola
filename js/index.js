$(function(){
	//加载头部和尾部信息
	$("#index_header").load("header.html?_"+Math.random());
	$("#index_footer").load("footer.html?_"+Math.random());
	
	
	//2.二级菜单
	$(".jCartMenu>li").mouseenter(function(){
		var index = $(this).index();

		$(this).parent().find(".cartList").hide();
		$(this).parent().find(".cartList").eq(index).show();
	}).mouseleave(function(){
		$(this).parent().find(".cartList").hide();
	})
	
	$(".cartList").mouseenter(function(){
		$(this).show();
	}).mouseleave(function(){
		$(this).hide();
	})
	
	//搜索框
	$("#nextSearch").click(function(){
		if ($("#inputSearch").val() != "") {
			if (/^iphone$/gi.test($("#inputSearch").val())) {
				window.location.href = "list.html";
			}
		}
	})
	
	$(".logo").click(function(){
		window.location.href = "index.html";
	})
	
	
	
	//轮播图插件
	var  index = 0; 
	show();  
	function show(){
		if(index==$('#bigpic').find('li').length){
			index =0;		
		}else if(index<0){				
			index=2;
		}
		$('#bigpic').find('li').eq(index).stop().animate({'opacity':1},200).siblings().stop().animate({'opacity':0},500);
		$('#smallpic').find('li').find("span").removeClass("active")
		$('#smallpic').find('li').eq(index).find("span").addClass("active")
	}
			
	var timer = setInterval(fAnimate,4000)
	function fAnimate(){
		index++;
		show();
	}
			
	$('#smallpic').find('li').mouseenter(function(){
		index = $(this).index();
		show();
	})			
	$('.prev').click(function(){
		index--;
		show();	
		return false;
	})
			
	$('.next').click(function(){
		index++;
		show();
		return false;
	})
	
	$('#focus').hover(function(){
		clearInterval(timer);
	},function(){
		timer = setInterval(fAnimate,4000);
	})
	
	
	//lunbo
	 var mySwiper = new Swiper ('.swiper-container', {
		direction: 'horizontal',
	    loop: true,
				    
	    // 如果需要分页器
	    pagination: '.swiper-pagination',
		autoplay : 1000,
		speed:300,	
	})
	 
	 
	 //3.楼梯滑动块
	 var $navScrollTop = $("#index_nav").offset().top + $("#index_nav").outerHeight();
	 var $indexList    = $("#index_List").offset().top;
	 var $leftTab      = $("#lTab");
	 var $rightTab     = $("#rTab");
//	 var $nav          = $(".floor");
	 var $floor        = $("#index_List>div").not("#list1,#guess");
	//console.log($nav.length);
	 $(window).scroll(function(){
	 	var scrollTop = $(window).scrollTop();
	 	if (scrollTop > $navScrollTop) {
	 		$("#index_fixbody").show();
	 	}else{
	 		$("#index_fixbody").hide();
	 	}
	 	
	 	if (scrollTop > $indexList) {
	 		$leftTab.removeClass("leftTab").addClass("leftFi");
	 		$rightTab.removeClass("rightPo").addClass("rightFi");
	 	} else{
	 		$leftTab.removeClass("leftFi").addClass("leftTab");
	 		$rightTab.removeClass("rightFi").addClass("rightPo");
	 	}
	 	

	 })
	 
	 //显示APP和签到文字
	 $("#flag_Tab").hover(function(){
	 	$(this).find(".flagTab").show();
	 },function(){
	 	$(this).find(".flagTab").hide();
	 })
	 $("#app_Tab").hover(function(){
	 	$(this).find(".appTab").show().css("background","white");
	 },function(){
	 	$(this).find(".appTab").hide();
	 })
	 
	
	 //楼梯
	$("#lTab").on("click",".floor",function(){
		var index = $(".floor").index(this);
	 	var top   = $floor.eq(index).offset().top - 50;
	 	console.log(index);
	 	$('html,body').animate({scrollTop:top});
	 	return false;
	})
	
	//返回顶部
	$("#toTop").click(function(){
		$('html,body').animate({scrollTop:0})
	})
	
})