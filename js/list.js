
$(function(){
	//1.加载头部和尾部信息
	$("#index_header").load("header.html?_"+Math.random(),function(){
		$("#headerCommon").attr("src","../js/common.js")
	});
	$("#index_footer").load("footer.html?_"+Math.random());
	
	
	//2.二级菜单
	$(".jCartMenu>li").mouseenter(function(){
		var index = $(this).index();
		$(this).parent().find("div").show();
	}).mouseleave(function(){
		$(this).parent().find("div").hide();
	})
	
	$(".logo").click(function(){
		window.location.href = "index.html";
	})
	
	//2.搜索框
	$("#nextSearch").click(function(){
		if ($("#inputSearch").val() != "") {
			if (/^iphone$/gi.test($("#inputSearch").val())) {
				window.location.href = "list.html";
			}
		}
	})	

	 //3.显示APP和签到文字
	 $("#flag_Tab").hover(function(){
	 	$(this).find(".flagTab").show();
	 },function(){
	 	$(this).find(".flagTab").hide();
	 })
	 $("#app_Tab").hover(function(){
	 	$(this).find(".appTab").show().css("background","white");
	 },function(){
	 	$(this).find(".appTab").hide();
	 })
	 	
	//4.返回顶部
	$("#toTop").click(function(){
		$('html,body').animate({scrollTop:0})
	})
	
	//5.收起
	$(".branmore").click(function(){
		$("#oTab").toggle();
	})
	


	

	//函数调用
	var _obj = {
		baseDom:'.goods',
		cloneSize: 12,
		url: '../json/list.json',
		pageContainer: '#pagination-demo1',
		page: true
	};

	$.ncBind(_obj,control);




	//3.边框变红
	function control(){
		$(".tocart").hide()
		$(".goodbook").mouseenter(function(){
			$(this).css({
				"border":"solid 1px red",
				"height":"472px"
			});
			$(this).parent().css("z-index",50);
			$(this).find("p").eq(1).removeClass("sale").addClass("saletile");
			$(this).find(".titleWarp").find("a").removeClass("notitle").addClass("title");
			$(this).find(".tocart").show();
		}).mouseleave(function(){
			$(this).css({
				"border":"solid 1px white",
				"height":"444px"
			});
			$(this).parent().css("z-index",1);
			$(this).find("p").eq(1).removeClass("saletile").addClass("sale");
			$(this).find(".titleWarp").find("a").removeClass("title").addClass("notitle");
			$(this).find(".tocart").hide();
		})
		
		//4.点击小图显示大图
		$(".smallImg").mouseover(function(){
			var index = $(this).index();
			
			$(this).siblings().find("img").css("border","solid 1px gray")
			$(this).find("img").css("border","solid 1px red");
			$(this).closest(".goods").find(".goodimg").find(".phoneimg").eq(index).show().siblings().hide()
		})	
	}


	//4.加入购物车
	$(".detaList").on("click",function(evt){
		if ($(evt.target).is(".tocart")) {
			var _data = $(evt.target).closest("li").data('model');
			var _guid = $(evt.target).closest("li").data('guid');

			_data.guid = _guid;
			var arry = [];
			var flag = true;

			if ($.cookie("cart")) {
				arry = JSON.parse($.cookie("cart"));
				$.each(arry,function(index,obj){
					if (obj.guid == _guid) {
						obj.count1 += 1;
						flag = false;
						return true;
					};
				})
			};

			if (flag) {
				_data.count1 = _data.count1 || 1;
				arry.push(_data);
			};

			//存取cookie
			$.cookie("cart",JSON.stringify(arry));
			console.log(arry);
		};
		
	})

})

