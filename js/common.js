//1.显示登录用户名
if ($.cookie("loginname")) {
	$("#welcome").html("欢迎    ,");
	$("#userName").html($.cookie("loginname"));
	$("#exit").html("退出");
}


//2.退出用户名
$("#exit").click(function() {
	if ($(this).html() == "退出") {
		$.removeCookie("loginname", {
			path: '/'
		});
		window.location.reload();
	}
})

//3.判断是否是购物车
if (window.location.pathname == "/%E4%BD%9C%E4%B8%9A/project/yang/kao/html/shopcar.html") {
	if ($("#userName").html() == '<a href="login.html">登录</a>') {
		window.location.href = "index.html";
	}
};

$("#header_Nav .mobile").mouseenter(function() {
	$(this).find(".app").show();
}).mouseleave(function() {
	$(this).find(".app").hide();
})

//user个人中心
$("#navList .caret").hover(function() {
	$(this).css("background", "white")
	$(this).find("ul").show();
	$(this).find("a").find(".icon").removeClass("fa-caret-down").addClass("fa-caret-up");
}, function() {
	$(this).find("ul").hide();
	$(this).find("a").find(".icon").removeClass("fa-caret-up").addClass("fa-caret-down");
	$(this).css("background", "#f2f2f2")
})

//more更多
$("#navList .more").mouseenter(function() {
	$(this).css("background", "white")
	$(this).find("ul").show();
}).mouseleave(function() {
	$(this).find("ul").hide();
	$(this).css("background", "#f2f2f2")
})

//微博
$("#navList #weibo").hover(function() {
		$(this).parents("ul").find(".wb").show()
	}, function() {
		$(this).parents("ul").find(".wb").hide()
	})
	//微信
$("#navList #weixin").hover(function() {
	$(this).parents("ul").find(".wx").show()
}, function() {
	$(this).parents("ul").find(".wx").hide()
})

//易信
$("#navList #yixin").hover(function() {
	$(this).parents("ul").find(".yx").show()
}, function() {
	$(this).parents("ul").find(".yx").hide()
})

//去购物车
$(".shopcar2").click(function(evt) {
	if ($("#userName").html() == '<a href="login.html">登录</a>') {
		//如果没有登录，阻止进入默认行为
		alert("警告", "请登录后才进入购物车", {
			type: 'error',
			confirmButtonText: '确定'
		})
		evt.preventDefault()
	}
})