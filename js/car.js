$(function() {
	//加载头部和尾部信息
	$("#shopHeader").load("header.html?_" + Math.random());
	$("#shopFooter").load("footer.html?_" + Math.random());



	var opts = {
		baseDom: '.shop',
		cloneSize: 5,
		page: true,
		pageContainer: '#pagination-demo1'
	}

	opts.data = JSON.parse($.cookie("cart")) || 　[];
	console.log(opts.data);
	if (opts.data) {
		$.ncBind(opts);
	};

	//3.设置每页显示商品的数量
	// var pageNum = 4;

	//向服务器发送请求
	// $.get("../json/car.json",function(data){
	// 	dispose(data,arry);
	// 	clievent();
	// })

	//处理函数
	function dispose(data, array) {
		var shopcar = ""
		$.each(array, function(index, value) {
			//遍历每一个独立的数据
			$.each(data, function(index1, value1) {
				//判断是否为当前的ID
				if (index1 == value) {
					$.each(value1, function(key, value2) {
						switch (key) {
							case "imgwarp":
								shopcar += '<ul class="shop" id="' + index1 + '"><li class="ware"><input type="checkbox" class="shopall allcheck1 u-chek" /><b>商家浙江仓发货</b></li><li class="actinfo"><span class="actLabel">满额减</span><span class="nextTure">再购<b>1499元</b>立享【满1499元减400元】<a class="actlink" href="#">去凑单 ></a> </span></li><li class="good clearfix"><div class="list1"><input type="checkbox" class="check  allcheck1 u-chek"  /></div><div class="clearfix list2"><a href="#" class="imgwarp"><img src="' + value2 + '"/></a>';
								break;
							case "goodtlt":
								shopcar += '<div class="textwarp"><h3 class="goodtlt"><a href="#">' + value2 + '</a></h3><p class="carcolor">颜色：黑色</p></div></div>';
								break;
							case "price":
								shopcar += '<div class="list3"><del class="oldprice">5100.00</del><span class="price">' + value2 + '</span></div><div class="list4"><span class="ctrnum"><a href="#" class="add mulit">+</a><input type="text" class="text1" value="1" /><a href="#" class="minus mulit">-</a></span><p>仅剩5件</p></div><div class="list5">个数：<span class="num">1</span><br />金额：<span class="sum">10</span></div><div class="list6"><a href="#" class="uopt delet">删除</a><p class="uopt">加入收藏</p></div></li><li class="total"><span class="discount">已选商品数：<span class="totalnum">0</span></span><span class="allsum"> 商品应付总计：<b>￥<b class="totalSum">0.00</b></b></span><span class="taxBox"> 预估税费：￥0.00</span></li></ul>'
								break;
							default:
								break;
						}
					});
				}
			});
		})
		$(".list").html(shopcar);

		page();
	}

	//设置整个页面的点击事件函数
	function clievent() {

		$(".car").on("click", function(evt) {
			//点击事件,重新再获取一下节点
			var $shopall = $(".shopall");
			var $check = $(".check");
			var $allcheck = $(".allcheck1");
			var $checkall = $(".checkall");

			//1.店铺全选：影响本店铺下面的所有单选和全选
			if ($(evt.target).is(".shopall")) {
				//单选
				var parent = $(evt.target).closest(".shop")
				var $check1 = $(".good .check", parent);
				$check1.prop("checked", $(evt.target).prop("checked"));

				//影响全选
				var shopallNum = $shopall.filter(":checked");
				$(".checkall").prop("checked", shopallNum.length == $shopall.length)
			}

			//2.单选框：影响本店铺的全选和间接影响整个全选
			if ($(evt.target).is(".check")) {
				//店铺全选
				var parent = $(evt.target).closest(".shop")
				var $check1 = $(".good .check", parent);
				var $check1Num = $check1.filter(":checked");
				$(".ware .shopall", parent).prop("checked", $check1Num.length == $check1.length);

				//全选
				var $checkNum = $check.filter(":checked");
				$(".checkall").prop("checked", $checkNum.length == $check.length)
			}



			//3.加法
			if ($(evt.target).is(".add")) {
				var $phone = $(evt.target).parent().parent().find(".text1");
				var num;
				if ($phone.val() == "" || isNaN($phone.val())) {
					num = 1;
				} else {
					num = parseInt($phone.val()) + 1;
				}
				$phone.val(num);
				evt.preventDefault();
			}

			//4.减法
			if ($(evt.target).is(".minus")) {
				var $phone = $(evt.target).parent().parent().find(".text1");
				var num;
				if ($phone.val() <= 1) {

					num = 1;
				} else {
					num = parseInt($phone.val()) - 1;
				}
				$phone.val(num);
				evt.preventDefault();
			}


			//5.点击获取数量和金额
			if ($(evt.target).is(".mulit")) {
				var parent = $(evt.target).closest(".good");

				var val = $(".list4 .text1", parent).val();
				var cash = $(".list3 .price", parent).html();


				var totalSum = parseInt(val) * parseInt(cash) + "";

				$(".list5 .num", parent).html(val);
				$(".list5 .sum", parent).html(totalSum);
			}

			//6.计算当前店铺总价
			//1定义一个装总数的变量，包括数目和金额
			var allnum = 0;
			var allsum = 0;
			var parent = $(evt.target).closest(".shop")
			var $check1 = $(".good .check", parent);
			$check1.each(function(index, value) {
				//2如果选中就加上整个购物车，没有就不用
				if (this.checked) {
					var parent = $(this).closest(".good");
					var num1 = $(".list5 .num", parent).html();
					var sum1 = $(".list5 .sum", parent).html();
					allnum += parseInt(num1);
					allsum += parseInt(sum1);

				}
			})

			//3添加数量进店铺总数，没有选择就是默认值
			var parent = $(evt.target).closest(".shop");
			$(".total .totalnum", parent).html(allnum);
			$(".total .totalSum", parent).html(allsum);


			//7.全选框
			if ($(evt.target).is(".checkall")) {

				$allcheck.prop("checked", $(evt.target).prop("checked"));
				$checkall.prop("checked", $(evt.target).prop("checked"));

				//不触发事件我就和你触发一个事件
				$shopall.each(function(index, value) {
					//1计算当前店铺总价遍历每个div
					var allnum = 0;
					var allsum = 0;
					var parent = $(value).closest(".shop")
					var $check1 = $(".good .check", parent);

					//2遍历地下每一个check
					$check1.each(function(index, value) {
						//3如果选中就加上整个购物车，没有就不用
						if (this.checked) {
							var parent = $(this).closest(".good");
							var num1 = $(".list5 .num", parent).html();
							var sum1 = $(".list5 .sum", parent).html();
							allnum += parseInt(num1);
							allsum += parseInt(sum1);
						}
					})

					//4加到总数那里
					var parent = $(value).closest(".shop");
					$(".total .totalnum", parent).html(allnum);
					$(".total .totalSum", parent).html(allsum);

				})
			}



			//9.点击删除事件
			if ($(evt.target).is(".delet")) {
				var parent = $(evt.target).closest(".shop");
				parent.remove();
				//存取当前商品的cookie
				var html = "";
				$(".shop").each(function(index, value) {
					html += $(value).attr("id") + "&";
				})
				$.cookie("good", html, {
					expires: 7,
					path: '/'
				});
				alert("通知", "删除该商品成功", function() {
					//1.点击更新数据
					$('.car').click();
				}, {
					type: 'success',
					confirmButtonText: 'OK'
				})
				evt.preventDefault();
			}


			//8.计算到总计
			//计算当前店铺总价
			//1.定义一个装总数的变量，包括数目和金额
			var allnum = 0;
			var allsum = 0;

			$shopall.each(function(index, value) {
				//2.如果选中就加上整个购物车，没有就不用

				var parent = $(this).closest(".shop");
				var num1 = $(".total .totalnum", parent).html();
				var sum1 = $(".total .totalSum", parent).html();
				allnum += parseInt(num1);
				allsum += parseInt(sum1);

			})

			//3.添加数量进店铺总数，没有选择就是默认值
			$("#carnum").html(allnum);
			$("#carsum").html(allsum);



			//10.点击按钮事件
			if ($(evt.target).is(".pageCar")) {
				//计算他们显示的区间范围
				var index = $(evt.target).index();
				var start = index * pageNum;
				var end = (index + 1) * pageNum;
				$(".shop").hide();
				$(".shop").slice(start, end).show();
				$(evt.target).css({
					"background": "red",
					"border": "1px solid red"
				}).siblings().css({
					"background": "white",
					"border": "1px solid #666"
				});
			}
		})
	}


	// //设置分页的函数
	// function page(){
	// 	var pageTotal = $(".shop").length;
	// 	var page      = Math.ceil(pageTotal/pageNum);
	// 	var html = "<div class = 'page'>"
	// 	for (var i=1;i<=page;i++) {
	// 		html += "<span class='pageCar'>"+i+"</span>"
	// 	}
	// 	html += "</div>";

	// 	$(".title").before(html);

	// 	//显示和隐藏
	// 	$(".shop").hide();
	// 	$(".shop").slice(0,pageNum).show();
	// 	$(".page>span").eq(0).css({
	// 		"background":"red",
	// 		"border":"1px solid red"
	// 	});
	// }

})