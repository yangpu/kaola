//封装jquery插件
;(function($){
	$.ncBind = function(opts,callback){
		//默认配置
		var _default = {
			baseDom: null,
			url: null, // 权重次于 data， 如果 data 为空，url 不为空的情况下，则 ajax 请求 url 解析出 data
			data: [], // 权重最高，如果 data 不为空则直接当数据源使用
			cloneSize: 0,
			page: false, //如果 page = true 的情况，要实现分页
			pageContainer: null
		}
		var $this = this;
		//对象合并，生成一个全新的对象,
		//后面的对象属性替换前面对象已有的属性，如果是新属性，则添加
		//深度克隆	
		$this.newObj = $.extend(_default, opts);
		

		//确定数据源
		var init = function(_callback){
			//如果数据源为空则不执行其它操作
			if(!$this.newObj.data && !$this.newObj.url){
				return false;
			}
			//如果 baseDom为空 或者 cloneSize 小于 0， 则不执行其它操作
			if(!$this.newObj.baseDom || $this.newObj.cloneSize < 1){
				return false;
			}
			//如果 data 不为空则把 data 当数据源操作
			if($this.newObj.data[0]){
				$this.newObj.data = !$this.newObj.data instanceof Array ? [$this.newObj.data] : $this.newObj.data;
			} else if($this.newObj.url)	{
				$.get($this.newObj.url + '?_=' + Math.random(), function(_response){
					var data = typeof _response == 'string' ? JSON.parse(_response) : _response;
					$this.newObj.data = data.data;
					if(_callback && typeof _callback == 'function'){
						_callback();
					}
				})
			}
			return true;
		}

		var generateHtml = function(_page){
			_page = _page || 1;
			//计算每页显示的数量
			var _pageSize = $this.newObj.cloneSize;
			//每页显示的数组最小下标
			var _min = (_page - 1) * _pageSize;
			//每页显示的数组最大下标
			var _max = _page * _pageSize -1;

			if(!$this.newObj.data[0]){
				return false;
			}
			$($this.newObj.baseDom).not(':first-child').remove();
			for(var i = _min; i <= _max; i++){

				if($this.newObj.data[i]){
					var _cloneDom = $($this.newObj.baseDom).eq(0).clone().appendTo($($this.newObj.baseDom).parent());
					_cloneDom.data('model',$this.newObj.data[i]).data('guid', $this.newObj.data[i].guid || Guid.NewGuid().ToString())
					$.each($('[nc-bind]', _cloneDom), function(_index, _element){
						if($(_element).is('img')){
							$(_element).attr('src', $this.newObj.data[i][$(_element).attr('nc-bind')]);
						} else {
							$(_element).text($this.newObj.data[i][$(_element).attr('nc-bind')]);
						}
					})
				}
			}
			$($this.newObj.baseDom).eq(0).remove();
		}

		var dkpage = function(){
			$($this.newObj.pageContainer).pagination({
	            dataSource: $this.newObj.data,
	            pageSize: $this.newObj.cloneSize,
	            callback: function (response, pagination) {
	            	$this.refresh(pagination.pageNumber);
	                // window.console && console.log(response, pagination);
	            }
	        });        
		}

		this.refresh = function(_page){
			//如果数据源 data 为空，而且 url 不为空，则定为需要 ajax 请求数据源
			if(!this.newObj.data[0] && this.newObj.url){
				//调用初始化方法并把生成 html 方法当回调函数执行
				// init(generateHtml);
				init(function(){
					generateHtml(_page);
					if($this.newObj.page){
						dkpage();
					};
					if (callback && typeof callback == "function") {
						callback();
					};					
				});
			} else if(this.newObj.data && !this.newObj.data instanceof Array){
				//直接调用数据初始化方法
				var _init = init();
				if(_init){
					//调用生成 html 的方法
					generateHtml(_page);
					if($this.newObj.page){
						dkpage();
					};
					if (callback && typeof callback == "function") {
						callback();
					};
				}	
			} else if(this.newObj.data && this.newObj.data instanceof Array){
				generateHtml(_page);
				if (callback && typeof callback == "function") {
					callback();
				};
			}
		}

		this.refresh(1);	
	};
})(jQuery);	
