$(function() {
	$("#loginFoot").load("footer.html?_" + Math.random());


	var COOKIE_NAME = 'username';
	var COOKIE_CHECK = "check";
	var COOKIE_PASSWORD = "password";
	var message = "";
	if ($.cookie(COOKIE_NAME)) {
		$("#account").val($.cookie(COOKIE_NAME));

	}
	if ($.cookie(COOKIE_CHECK)) {
		$("#check").prop("checked", $.cookie(COOKIE_CHECK));
	}
	if ($.cookie(COOKIE_PASSWORD)) {
		$("#password").val($.cookie(COOKIE_PASSWORD));
	}

	if ($.cookie("message")) {
		message = $.cookie("message");
		message = typeof message == "string" ? JSON.parse(message) : message;
	}
	console.log(message.account);
	console.log(message.password);


	//2.判断是否保存cookie页面信息
	$("#check").click(function() {
		var $check = $("#check").prop("checked");
		var $account = $("#account").val();
		var $password = $("#password").val();
		if (!this.checked) {
			//删除cookie 
			$.removeCookie(COOKIE_NAME, {
				path: '/'
			});
			$.removeCookie(COOKIE_CHECK, {
				path: '/'
			});
			$.removeCookie(COOKIE_PASSWORD, {
				path: '/'
			});
			console.log("删除cookie")
		} else {
			$.cookie(COOKIE_NAME, $account, {
				expires: 7,
				path: '/'
			});
			$.cookie(COOKIE_CHECK, $check, {
				expires: 7,
				path: '/'
			});
			$.cookie(COOKIE_PASSWORD, $password, {
				expires: 7,
				path: '/'
			});
			console.log("保存cookie")
		}
	})



	//3.登录时保持cookie
	$("#signto").click(function() {
		var $check = $("#check").prop("checked");
		var $account = $("#account").val();
		var $password = $("#password").val();

		if ($check) {
			$.cookie(COOKIE_NAME, $account, {
				expires: 7,
				path: '/'
			});
			$.cookie(COOKIE_CHECK, $check, {
				expires: 7,
				path: '/'
			});
			$.cookie(COOKIE_PASSWORD, $password, {
				expires: 7,
				path: '/'
			});
			console.log("保存cookie")
		}

		if ($account == message.account && $password == message.password) {
			//显示遮罩
			if ($('.mask')[0]) {
				$('.mask').show();
			} else {
				$('<div class="mask"><i class="fa fa-spinner fa-spin"></i></div>').appendTo('body').show();
			}

			//保存cookie
			$.cookie("loginname", $account, {
				expires: 7,
				path: '/'
			});

			setTimeout(function() {
				$('.mask').hide();
				alert("Hello world!", "welcome to kaola", function() {
					window.location.href = "index.html";
				}, {
					type: 'success',
					confirmButtonText: 'OK'
				});

			}, 2000)
		} else {
			if ($account != message.account) {
				alert("错误", "用户名错误", {
					type: 'error',
					confirmButtonText: '确定'
				});
			} else if ($password != message.password) {
				alert("错误", "密码错误", {
					type: 'error',
					confirmButtonText: '确定'
				});
			}
		}
		return false;
	});


})