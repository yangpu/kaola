$(function(){
	//1.加载头部和尾部信息
	$("#index_header").load("header.html?_"+Math.random());
	$("#index_footer").load("footer.html?_"+Math.random());
	
	
	//2.二级菜单
	$(".jCartMenu>li").mouseenter(function(){
		var index = $(this).index();
		$(this).parent().find(".cartList").hide();
		$(this).parent().find(".cartList").eq(index).show();
	}).mouseleave(function(){
		$(this).parent().find(".cartList").hide();
	})
	
	$(".cartList").mouseenter(function(){
		$(this).show();
	}).mouseleave(function(){
		$(this).hide();
	})
	
	//2.搜索框
	$("#nextSearch").click(function(){
		if ($("#inputSearch").val() != "") {
			if (/^iphone$/gi.test($("#inputSearch").val())) {
				window.location.href = "list.html";
			}
		}
	})
	
	$(".logo").click(function(){
		window.location.href = "index.html";
	})	
	
	//3.所有分类
	$("#topCart").mouseenter(function(){
		$(this).next().show();
	}).mouseleave(function(){
		$(this).next().hide();
	})
	
	$(".jCartMenu").mouseover(function(){
		$(this).show();
	}).mouseout(function(){
		$(this).hide();
	})
	
	 //4.显示APP和签到文字
	 $("#flag_Tab").hover(function(){
	 	$(this).find(".flagTab").show();
	 },function(){
	 	$(this).find(".flagTab").hide();
	 })
	 $("#app_Tab").hover(function(){
	 	$(this).find(".appTab").show().css("background","white");
	 },function(){
	 	$(this).find(".appTab").hide();
	 })
	 	
	//5.返回顶部
	$("#toTop").click(function(){
		$('html,body').animate({scrollTop:0})
	})
	
	var swiper = new Swiper('.swiper-container', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 4,
        spaceBetween: 30
    });
			
	
	//7显示城市
	$("#notage2").click(function(){
		$(this).parent().find(".alladd").toggle();
	})
	
	//8.显示信息
	$(".sendfree").hover(function(){
		$(this).find(".desc").show();
	},function(){
		$(this).find(".desc").hide();
	})
	
	//9.二级菜单的功能
	layui.use('element', function(){
		  var element = layui.element(); 
		  //导航的hover效果、二级菜单等功能，需要依赖element模块
		  
		  //监听导航点击
		  element.on('nav(demo)', function(elem){
		    layer.msg(elem.text());
		  });
	});
	
	//10。选择样式
	var color = false;
	var product = "";
	$(".neicun").on("click",".g32",function(){
		$(".g32").css("border-color","#666666");
		$(this).css("border","1px solid #D41C44");
		color = true;
		product = $(this).html();
	})
	
	//11.选择品牌
	var imgStyle = false;
	var img = "";
	var $phone = $("#phoneNum");
	$(".color").on("click",".mobilephone",function(){
		$(".mobilephone").css("border","1px #666666 solid");
		$(this).css("border","1px solid #D41C44");
		if ($phone.val() == "") {
			$phone.val(1);
		}
		img = $(this).find("img").attr("src");
		imgStyle = true;
		return false;
	})
	
	//12.数量的加减
	$("#add").click(function(){
		var num;
		if ($phone.val() == "" || isNaN($phone.val())) {
			num = 1;
		} else{
			num = parseInt($phone.val())+1;
		}
		$phone.val(num);
		return false;
	})
	$("#minus").click(function(){
		var num;
		if ($phone.val() <= 1) {

			num = 1;
		} else{
			num = parseInt($phone.val()) - 1;
		}
		$phone.val(num);
		return false;
	})
	
	//13.加入购物车
	$("#favbtn").click(function(evt){
		if (color && imgStyle && !isNaN($phone.val()) && $phone.val() != "" && $("#userName").html() != '<a href="login.html">登录</a>') {
			var total = parseInt($(".money").html());
			var num   = parseInt($phone.val())

		    $.cookie("title",$(".phonethem").html(),{ expires: 7, path: '/' });
		    $.cookie("product", product,{ expires: 7, path: '/' });
		    $.cookie("img",img,{ expires: 7, path: '/' });
		    $.cookie("number",$phone.val(),{ expires: 7, path: '/' });
		    $.cookie("newprice", $(".money").html(),{ expires: 7, path: '/' });
		    $.cookie("oldprice",$(".money1").html(),{ expires: 7, path: '/' });

			
			confirm("该商品已成功放入购物车", "购物车共" +$phone.val() +"件商品 合计：$" + total*num + "元", function (isConfirm) {
                if (isConfirm) {
                    //after click the confirm
                } else {
                    window.location = "shopcar.html";
                }
            }, {confirmButtonText: '继续购物', cancelButtonText: '去结算', width: 400,type:'success'});

			
   
		} else{
			alert("加入购物车失败", "请选择完整的商品信息或者登录后在添加", function () {
	            }, {type: 'error', confirmButtonText: '确定'});
		}
		return false;
	})
	
	//14.评论和详情
	$(".commentdetail").hide();
	$(".commentNav").on("click","span",function(){
		var index =  $(this).index();
		$(this).addClass("active").siblings().removeClass("active");
		$(".tab1").hide();
		$(".tab1").eq(index).show();
	})
	
	
	//15评论功能
	var $text = $("#textphone")
	$("#phoneBtn").click(function(){
		
		if ($text.val().length>0) {
			 var date = new Date()
			 var mon = date.getMonth()+1;
			 var day = date.getDate();
			var comment = '<li class="clearfix"><div class="comment1"><div class="avater"></div><p>*****我****</p></div><div class="comment2"><i class="fa fa-star"></i><i class="fa fa-star"></i><span>内存容量：32GB 颜色：玫瑰金色</span><br /><br /><span class="commentitem">我的评论</span>' + $text.val() + '</div><div class="comment3"><div class="thumup"><i class="fa fa-thumbs-up"></i>8</div><p><a href="#">来自电脑网页版</a></p><p>' + mon + '.' + day +'</p></div></li>'
			$("#listphone").prepend(comment);
		} else{
			alert("发送失败", "请输入相关评论", function () {
	            }, {type: 'error', confirmButtonText: '确定'});
		}
		
	})
	
})

