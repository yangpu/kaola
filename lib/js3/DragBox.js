

/**
 * 构造函数创建拖拽对象
 * 属性：需要退拽的元素节点ele
 * 方法：startMove()
 *     moving()
 *     stopMove()
 */

   function DragBox(id,it){
   	 
   	  if(id==undefined){
   	  	return;
   	  }
   	  
   	  //属性ele
   	  this.ele = document.getElementById(id);
   	  this.eleparent = document.getElementById(it)
   	  
   	  var self = this;
   	  
   	  //给ele添加鼠标按下去的事件
   	  this.ele.onmousedown = function(evt){
   	  	   var oEvent = evt ||　event;
   	  	    self.startMove(oEvent.clientX,oEvent.clientY);
   	  	   self.eleparent.innerText = "";
   	  }
   }
   
   
   DragBox.prototype.startMove = function(x,y){
   	
   		this.disX = x - this.ele.offsetLeft;
   		this.disY = y - this.ele.offsetTop;
   		
   		var self = this;
   		
   		document.onmousemove = function(evt){
   			var oEvent = evt || event;
   			self.moving(oEvent.clientX,oEvent.clientY);
   		}
   		
   		document.onmouseup = function(){
   			
   			self.stopMove();
   		}
   	
   }
   
   DragBox.prototype.moving = function(x,y){
   	 	var left = x - this.disX;
    	var top  = y - this.disY;
    	
    	if(left<0){
    		left = 0;
    	}
    	
    	if (left>150) {
    		left = 250;
    	}
    	
    	this.ele.style.left = left + "px";
    	this.ele.style.top = 0 + "px";
    	
   }
   
   DragBox.prototype.stopMove = function(){
   	  document.onmousemove = null;
   	  document.onmouseup   = null;
   	
   }
   
