


//构造函数
  function DragBoxLimitText(id){
  	   //借用构造函数
  	   DragBoxLimit.call(this,id);
  }
  
  //原型链继承
  DragBoxLimitText.prototype = new DragBoxLimit();
  
  //重写moving方法，显示坐标
  
    DragBoxLimitText.prototype.moving = function(x,y){
    	//借用DragBoxLimit原型的moving方法
    	DragBoxLimit.prototype.moving.call(this,x,y);
    	
    	//显示坐标的位置
    	this.ele.innerHTML = "left:"+this.ele.style.left+",top:"+this.ele.style.top;
    }
